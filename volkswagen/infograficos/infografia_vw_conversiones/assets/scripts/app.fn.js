var app = {
    dataUrl: "data/data.json",
    imgPath: "assets/images",
    data:undefined,
    /* INIT APP */
    init: function () {
      app.loadData();
    },
    loadData: function(){
        $.getJSON(app.dataUrl, function( data ) {
            app.data = data;
            var i = 0;
            $.each( data, function( key, val ) {
                $.each(val, function(j, sectionData){
                    $(".page section ul").eq(i).append('<li data-value='+ sectionData.id +'>'+sectionData.name+'</li>');
                });
                i++;
            });
            $.each($("section"), function(key, val){
                var first = $(val).find("li")[0];
                console.log($(first).attr("data-value"));
                app.updateSection(val, $(first).attr("data-value"));
            });
            app.handleListeners();
        });
    },
    /* HANDLERS */
    handleListeners: function () {

        $("ul li").click(function(e){
            e.preventDefault();
            var selected = $(this).attr("data-value");
            app.updateSection($(this).parents("section"), selected);
            $("section ul").hide();
        });

        $(".page .title").click(function(e){
            e.preventDefault();
            var selected = $(this).parent().find("ul");
            console.log(selected.css("display"));
            if(selected.css("display") == "none"){
                $("section ul").hide();
                selected.show();
            }else{
                $("section ul").hide();
            }
        });

    },
    updateSection: function(section, selected){
        $(section).find("img").attr("src", app.imgPath + "/coche" + selected  +  ".png");
        $.each( app.data, function( key, val ) {
            $.each(val, function(j, sectionData){
                if(sectionData.id == selected){
                    $(section).find("h3").html(sectionData.name);
                    $(section).find(".desc").html(sectionData.desc);
                }
            });
        });
    }
};




