
//Function for equal heights

function equalizeHeights(){
  $('.equalize').each(function(){

    var items    = $(this).find('.equalize-item');
    
    items.removeAttr("style");

    var maxHeight = Math.max.apply(Math, items.map(function(){
         return $(this).outerHeight(true);
    }).get());

    items.css('height', maxHeight);
  });
}