var window;

$(function(){
	
	window = $(window);
	window.tCounter = 0;

	$('.slider').slick({
	  variableWidth: false,
	  infinite: false,
	  dots: true,
	  lazyLoad: 'progressive',
	  prevArrow: '<button type="button" class="slick-prev"><span class="icon icon-prev"></span></button>',
	  nextArrow: '<button type="button" class="slick-next"><span class="icon icon-next">Avanzar</span></button>',
	  fade: true,
	  cssEase: 'linear'
	});

	//// LIGHTBOX

	// Lightbox type image
	$('.js-lightbox-image').magnificPopup({
		type:'image',
		closeMarkup: '<button title="%title%" class="mfp-close"><span class="icon icon-next"></span></button>'
	});

	$('#carga .objblink').hover(
	  function() {
	    $("#carga").addClass( "is-popup" );
	  }, function() {
	    $("#carga").removeClass( "is-popup" );
	  }
	);

	$('#trasera-b .objblink').hover(
	  function() {
	    $("#trasera-b").addClass( "is-popup" );
	  }, function() {
	    $("#trasera-b").removeClass( "is-popup" );
	  }
	);
	
	$('#lateral .objblink').hover(
	  function() {
	    $(".slick-next").hide();
	  }, function() {
	    $(".slick-next").show();
	  }
	);

	$('#lateral .rodaje .objblink').hover(
	  function() {
	    $("#lateral").addClass( "is-popup" );	    
	  }, function() {
	    $("#lateral").removeClass( "is-popup" );
	  }
	);

	$('#lateral .objblink').hover(
	  function() {
	    $(".info").hide();
	  }, function() {
	    $(".info").show(); 
	  }
	);

	$('#cabina .objblink').hover(
	  function() {
	    $(".info").hide();
	  }, function() {
	    $(".info").show(); 
	  }
	);

	$('#cabina .objblink').hover(
	  function() {
	    $("#cabina").addClass( "is-popup" );
	  }, function() {
	    $("#cabina").removeClass( "is-popup" );
	  }
	);

	$(".icon-volver").on("click", function(){
		$('.slider').slickGoTo(0);
	});
/*
	$('#trasera .objblink').hover(
	  function() {
	  	$("#trasera").css("background", "url('assets/images/bg05b.jpg') no-repeat top center");
	  	
	    window.tInterval = setInterval(function(){ 

	    	if(window.tCounter >= 5) 
	    		window.tCounter = 0;
	    	else
	    		window.tCounter++;

	    	$("#trasera .alturas-traseras").hide();
			$("#trasera .apertura-puertas").hide();
			$("#trasera .altura-suelo").hide();
			$("#trasera .trasera01").hide();
	    	$("#trasera .trasera02").hide();

	    	switch(window.tCounter){
	    		case 1:
					$("#trasera .alturas-traseras").show();
	    		break;
	    		case 2:
	    			$("#trasera .apertura-puertas").show();
	    		break;
	    		case 3:
	    			$("#trasera .altura-suelo").show();
	    		break;
	    		case 4:
	    			$("#trasera .trasera01").show();
	    		break;
	    		case 5:
	    			$("#trasera .trasera02").show();
	    		break;
	    	}
	    	
		}, 1400);
	  }, function() {
	  	window.tCounter = 0;
	    clearInterval(window.tInterval);
	    $("#trasera").css("background", "url('assets/images/bg05.jpg') no-repeat top center");
	    $("#trasera .alturas-traseras").hide();
		$("#trasera .apertura-puertas").hide();
		$("#trasera .altura-suelo").hide();
		$("#trasera .trasera01").hide();
	    $("#trasera .trasera02").hide();
	  }
	);
*/
	
	$("slick-prev, .slick-next, .slick-dots li button").on("click", function(){
		setTimeout(function(){ 
			var pos = $(".slick-active").index();
			if(pos == 7) 
				$(".slick-next").hide();
			else if(pos == 1){
				 setTimeout(function(){ 
				 	$("#motor").addClass("motor-bg");
				 }, 1200);
			}				
			else
				$(".slick-next").show();
		}, 200);		
	});



});
