<?php 
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
          <!-- info post -->

          <?php 
          $the_query = post_index();

          if ($the_query["recent"]->have_posts()) {
            $recent = [];
            while ( $the_query["recent"]->have_posts() ) {
              $the_query["recent"]->the_post();
              $post->post_excerpt = get_the_excerpt();
              $post->guid = get_the_permalink();
              $post->img_medium = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' )[0];
              $post->img_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' )[0];
              array_push($recent, $post);
            }
          }

          if ($the_query["highlights"]->have_posts()) {
            $highlights = [];
            while ( $the_query["highlights"]->have_posts() ) {
              $the_query["highlights"]->the_post();
              $post->post_excerpt = get_the_excerpt();
              $post->guid = get_the_permalink();
              $post->img_large = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' )[0];
              $post->img_thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' )[0];
              array_push($highlights, $post);
            }
          }
          wp_reset_query();                 
          ?>                               
          <div class="content-home padding-container">
            <h3 class="move-your-business">#MueveTuNegocio</h3>
            <section class="row">
              <!-- highlights mobile -->
              <article class="col-xx-1 col-xs-6 col-md-3 visible-mobile">
                  <figure>
                    <!-- src="http://placehold.it/257x257" -->
                    <img class="img-responsive" src="<?= $highlights[0]->img_thumbnail; ?> ">
                    <figcaption>
                      <?= $highlights[0]->post_title; ?>
                    </figcaption>
                    <a href="<?= $highlights[0]->guid; ?>">
                      <div class="excerpt">
                        <h4> <?= $highlights[0]->post_title; ?></h4>
                        <p>
                          <?= $highlights[0]->post_excerpt; ?>                        
                        </p>
                        <i class="view-more"></i>
                      </div>
                    </a>                
                  </figure>
              </article>               
              <!-- highlights mobile -->
              <article class="col-xx-1 col-xs-6 col-md-3">
                  <figure>
                    <!-- src="http://placehold.it/257x257" -->
                    <img class="img-responsive" src="<?= $recent[0]->img_thumbnail; ?> ">
                    <figcaption>
                      <?= $recent[0]->post_title; ?>
                    </figcaption>
                    <a href="<?= $recent[0]->guid; ?>">
                      <div class="excerpt">
                        <h4> <?= $recent[0]->post_title; ?></h4>
                        <p>
                          <?= $recent[0]->post_excerpt; ?>                        
                        </p>
                        <i class="view-more"></i>
                      </div>
                    </a>
                  </figure>
              </article>
              <article class="col-xx-1 col-xs-6 col-md-3">
                  <figure>
                    <!-- src="http://placehold.it/257x257" -->
                    <img class="img-responsive" src="<?= $recent[1]->img_thumbnail; ?> ">
                    <figcaption>
                      <?= $recent[1]->post_title; ?>
                    </figcaption>
                    <a href="<?= $recent[1]->guid; ?>">
                      <div class="excerpt">
                        <h4> <?= $recent[1]->post_title; ?></h4>
                        <p>
                          <?= $recent[1]->post_excerpt; ?>                        
                        </p>
                        <i class="view-more"></i>
                      </div>
                    </a>
                  </figure>
              </article>
              <article class="col-xx-1 col-xs-6 col-md-3">
                  <figure>
                    <!-- src="http://placehold.it/257x257" -->
                    <img class="img-responsive" src="<?= $recent[2]->img_thumbnail; ?> ">
                    <figcaption>
                      <?= $recent[2]->post_title; ?>
                    </figcaption>
                    <a href="<?= $recent[2]->guid; ?>">
                      <div class="excerpt">
                        <h4> <?= $recent[2]->post_title; ?></h4>
                        <p>
                          <?= $recent[2]->post_excerpt; ?>                        
                        </p>
                        <i class="view-more"></i>
                      </div>
                    </a>
                  </figure>
              </article>
              <article class="col-xx-1 col-xs-6 col-md-3 hidden-mobile">
                  <figure>
                    <!-- src="http://placehold.it/257x257" -->
                    <img class="img-responsive" src="<?= $recent[3]->img_thumbnail; ?> ">
                    <figcaption>
                      <?= $recent[3]->post_title; ?>
                    </figcaption>
                    <a href="<?= $recent[3]->guid; ?>">
                      <div class="excerpt">
                        <h4> <?= $recent[3]->post_title; ?></h4>
                        <p>
                          <?= $recent[3]->post_excerpt; ?>                        
                        </p>
                        <i class="view-more"></i>
                      </div>
                    </a>
                  </figure>
              </article>                    
            </section> 

            <section class="row">            
                <article class="col-md-6 hidden-mobile">
                    <figure class="main-post">
                      <!-- src="http://placehold.it/521x520" -->
                      <img class="img-responsive" src="<?= $highlights[0]->img_large; ?>">
                      <figcaption><?= $highlights[0]->post_title; ?></figcaption>
                      <a href="<?= $highlights[0]->guid; ?>">
                        <div class="excerpt">
                          <h4> <?= $highlights[0]->post_title; ?></h4>
                          <p>
                            <?= $highlights[0]->post_excerpt; ?>
                          </p>
                          <i class="view-more"></i>
                        </div>
                      </a>
                    </figure>
                </article>
                <div class="col-md-6">
                  <div class="row">
                    <article class="col-md-12 hidden-mobile">
                        <figure class="long-post">
                          <!-- src="http://placehold.it/521x255" -->
                          <img class="img-responsive" src="<?= $recent[4]->img_medium; ?>">
                          <figcaption><?= $recent[4]->post_title; ?></figcaption>
                          <a href="<?= $recent[4]->guid; ?>">
                            <div class="excerpt">
                              <h4><?= $recent[4]->post_title; ?></h4>
                              <p>
                                <?= $recent[4]->post_excerpt; ?>
                              </p>
                              <i class="view-more"></i>
                            </div>
                          </a>
                        </figure>
                    </article>
                    <div class="col-md-12">
                      <div class="row">
                      
                        <article class="col-xx-1 col-xs-6">
                          <figure>
                            <!-- src="http://placehold.it/265x265" -->
                            <img class="img-responsive" src="<?= $recent[5]->img_thumbnail; ?> ">
                            <figcaption><?= $recent[5]->post_title; ?></figcaption>
                            <a href="<?= $recent[5]->guid; ?>">
                              <div class="excerpt">
                                <h4> <?= $recent[5]->post_title; ?></h4>
                                <p>
                                  <?= $recent[5]->post_excerpt; ?>
                                </p>
                                <i class="view-more"></i>
                              </div>
                            </a>
                          </figure>
                        </article>
                        <article class="col-xx-1 col-xs-6 margin-bottom-mobile">
                          <figure>
                            <!-- src="http://placehold.it/265x265" -->
                            <img class="img-responsive" src="<?= $recent[6]->img_thumbnail; ?> ">
                            <figcaption><?= $recent[6]->post_title; ?></figcaption>
                            <a href="<?= $recent[6]->guid; ?>">
                              <div class="excerpt">
                                <h4> <?= $recent[6]->post_title; ?></h4>
                                <p>
                                  <?= $recent[6]->post_excerpt; ?>
                                </p>
                                <i class="view-more"></i>
                              </div>
                            </a>
                          </figure>
                        </article>
                       
                      </div>
                    </div>                                  
                  </div>       
                </div>
            </section> 
            <section class="row">
                <article class="col-xx-1 col-xs-6 col-md-3">
                    <figure>
                      <!-- src="http://placehold.it/257x257" -->
                      <img class="img-responsive" src="<?= $recent[7]->img_thumbnail; ?>">
                      <figcaption><?= $recent[7]->post_title; ?></figcaption>
                      <a href="<?= $recent[7]->guid; ?>">
                        <div class="excerpt">
                          <h4> <?= $recent[7]->post_title; ?></h4>
                          <p>
                            <?= $recent[7]->post_excerpt; ?>
                          </p>
                          <i class="view-more"></i>
                        </div>
                      </a>
                    </figure>
                </article>
                <article class="col-md-6 hidden-mobile">
                    <figure class="long-post">
                      <!-- src="http://placehold.it/521x257" -->
                      <img class="img-responsive" src="<?= $recent[8]->img_medium; ?>">
                      <figcaption><?= $recent[8]->post_title; ?></figcaption>
                      <a href="<?= $recent[8]->guid; ?>">
                        <div class="excerpt">
                          <h4><?= $recent[8]->post_title; ?></h4>
                          <p>
                            <?= $recent[8]->post_excerpt; ?>
                          </p>
                          <i class="view-more"></i>
                        </div>
                      </a>
                    </figure>
                </article>
                <article class="col-xx-1 col-xs-6 col-md-3">
                    <figure>
                      <!-- src="http://placehold.it/257x257" -->
                      <img class="img-responsive" src="<?= $recent[9]->img_thumbnail; ?>">
                      <figcaption><?= $recent[9]->post_title; ?></figcaption>
                      <a href="<?= $recent[9]->guid; ?>">
                        <div class="excerpt">
                          <h4> <?= $recent[9]->post_title; ?></h4>
                          <p>
                            <?= $recent[9]->post_excerpt; ?>
                          </p>
                          <i class="view-more"></i>
                        </div>
                      </a>
                    </figure>
                </article>      
            </section>  
          </div>