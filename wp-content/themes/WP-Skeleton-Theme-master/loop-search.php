<?php 
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
          <div class="category padding-container">
            <?php while ( have_posts() ) : the_post(); ?>
              <article class="row post-category">            
                  <div class="col-md-4">            
                    <figure>
                      <?php  $src = get_template_directory_uri().'/includes/scripts/timthumb.php?src='.wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'medium' )[0].'&h=190&w=345'; ?>                      
                      <a href="<?php the_permalink(); ?>"><img class="img-responsive" src="<?= $src ?>"></a>
                      <figcaption><?= get_the_category()[0]->name; ?></figcaption>
                    </figure>
                  </div>
                  <div class="col-md-8">
                    <div class="info-post">
                      <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                      <p><?php the_excerpt(); ?></p>
                      <div class="social-networks">
                        <span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>                        
                        <span class='st_twitter_large' st_via="" st_msg="<?php the_title(); ?> #MueveTuNegocio" st_title="|" st_url='<?php the_permalink(); ?>'></span>
                        <span class='st_linkedin_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        <span class='st_email_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                      </div>
                    </div>
                  </div>
              </article>
            <?php endwhile; ?>                     
          </div>