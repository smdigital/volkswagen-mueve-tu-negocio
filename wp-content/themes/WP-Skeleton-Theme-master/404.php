<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */

get_header(); 
get_template_part( 'menu', 'index' ); //the  menu + logo/site title ?>

    <div class="padding-container">
        <h2 class="title-general">404</h2>
        <p>Recurso no encontrado.</p>
        <form  method="get" id="searchform" action="<?php bloginfo('url'); ?>/" class="search">
          <input type="text" value="<?php the_search_query(); ?>" name="s" id="s">
          <input type="submit" value="Buscar">
        </form>
    </div>

<?php get_footer(); ?>