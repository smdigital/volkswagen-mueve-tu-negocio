<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
          <div class="internal-post padding-container">
            <?php while ( have_posts() ) : the_post(); ?>
            <section class="row">
              <div class="col-sm-6">
                <h3 class="breadcrumbs">
                  <?php
                    $numCat = get_category_by_slug( $_GET['cat'] )->term_id;
                    $paramCat = ($numCat) ? $numCat : get_the_category( $post->ID )[0]->term_id;
                  ?>
                  <?php $breadcrumbs = breadcrumbs_category($paramCat); ?>
                  <?= $breadcrumbs['link']; ?>
                </h3>
              </div>
              <div class="col-sm-6">
                <div class="social-networks">

                  <?php
                      // do_action( 'addthis_widget', get_permalink(), get_the_title(), array(
                      // 'type' => 'custom',
                      // 'size' => '32', // size of the icons.  Either 16 or 32
                      // 'services' => 'facebook,twitter,linkedin,email', // the services you want to always appear
                      // ));
                  ?>
                  <a href="http://www.volkswagen-comerciales.com/es/models.html" target="_blank" class="button-white right-models">Ver Modelos</a>
                  <span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?><?= ($numCat) ? "?cat=".$_GET["cat"] : "" ?>'></span>
                  <!-- <span class='st_twitter_large' displaytext='#MueveTuNegocio' st_via="" st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span> -->
                  <span class='st_twitter_large' st_via="" st_msg="<?php the_title(); ?> #MueveTuNegocio" st_title="|" st_url='<?php the_permalink(); ?><?= ($numCat) ? "?cat=".$_GET["cat"] : "" ?>'></span>
                  <span class='st_linkedin_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?><?= ($numCat) ? "?cat=".$_GET["cat"] : "" ?>'></span>
                  <span class='st_email_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?><?= ($numCat) ? "?cat=".$_GET["cat"] : "" ?>'></span>

                  <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/shared.jpg"> -->
                </div>
              </div>
            </section>


            <?php if (get_field('imagen_interna')): ?>
              <figure class="internal-image">
                <!-- http://placehold.it/1050x515 -->
                <img class="img-responsive" src="<?= get_field('imagen_interna')["sizes"]["internal"]; ?>">
                <figcaption>
                  <?php the_title(); ?>
                </figcaption>
              </figure>
            <?php else: ?>
              <h3 class="title-principal"><?php the_title(); ?></h3>
              <?php if ($url_ext = get_field('url_externa')): ?>
                <?= $url_ext ?>
              <?php endif ?>
            <?php endif ?>

            <section class="row">

                <section class="col-md-8 content-post">

                  <?php if (get_field('galeria')): ?>

                    <?php $gallery = get_field('galeria'); ?>
                    <div class="post-gallery">
                        <!-- Imagen completa -->
                        <ul class="bxbanner">
                            <?php foreach ($gallery as $value) {
                                  $imgGallery = wp_get_attachment_image_src( $value->ID, $size='large');
                                  $imgFull = wp_get_attachment_image_src( $value->ID, $size='full');

                                  echo '<li><a href="'.$imgFull[0].'" class="lightbox" title="'.$value->post_content.'" rel="gallery"><figure><img src="'.$imgFull[0].'"><figcaption>'.$value->post_content.'</figcaption></figure></a></li>';

                            } ?>
                        </ul>
                        <!-- Imagen completa -->
                    </div>
                    <div class="post-gallery-all">
                        <div class="gallery" id="bx-pager">
                            <!-- Carrusel de imagenes -->
                            <?php $numIndex = 0; ?>
                            <?php foreach ($gallery as $value) {
                                  $imgGallery = wp_get_attachment_image_src( $value->ID, $size='thumbnail');
                                  $imgFull = wp_get_attachment_image_src( $value->ID, $size='full');
                                  echo '<a data-slide-index="'.$numIndex++.'" href=""><img src="'.$imgGallery[0].'"></a>';
                            } ?>
                            <!-- Carrusel de imagenes -->
                        </div>
                    </div>
                  <?php endif ?>

                  <?php if (get_field('creditos')): ?>
                      <?= get_field('creditos'); ?>
                  <?php endif ?>
                  <?php the_content(); ?>

                  <?php if (is_user_logged_in()): ?>
                      <a href="javascript:window.print()" rel="nofollow"><img src="<?= get_template_directory_uri() ?>/img/icon/imprimir.png"></a>
                  <?php endif ?>

                  <?php $permalink = get_the_permalink(); ?>

                </section>
            <?php endwhile; ?>
                <section class="col-md-4">
                    <aside>
                      <div class="newsletter-subscription">
                        <div class="bg-silver">
                          <h3>¿Te gustó este artículo?</h3>
                          <p>Inscríbete en nuestro boletín de noticias y recibe actualizaciones directamente en tu email.</p>
                          <?php
                            $widgetNL = new WYSIJA_NL_Widget(true);
                            echo $widgetNL->widget(array('form' => 1, 'form_type' => 'php'));
                          ?>
                        </div>
                      </div>
                      <div class="internal-aside">
                        <h3>¡Lo más leído!</h3>

                          <?php $most_viewed = most_viewed();?>
                          <?php if( $most_viewed->have_posts() ): ?>
                              <?php $number_viewed = 1; ?>
                              <?php while ( $most_viewed->have_posts() ) : $most_viewed->the_post(); ?>
                                <article class="more-read">
                                  <h4><a href="<?php the_permalink(); ?>"><?=$number_viewed++?>. <?php the_title(); ?></a></h4>
                                  <p><?= cut_text(get_the_excerpt(),6); ?></p>
                                </article>
                              <?php endwhile; ?>
                          <?php endif; ?>
                          <?php wp_reset_query();?>
                      </div>
                      <div class="cotizador">
                        <div class="border-silver">
                          <h3>¿Estás buscando un vehículo que  mueva tu negocio?</h3>
                          <a href="http://www.volkswagen-comerciales.com/es/Cotizador.html" target="_blank" class="button-blue">Cotízalo Aquí</a>
                        </div>
                      </div>
                    </aside>
                </section>
            </section>


            <h3 class="title-internal">También te puede interesar</h3>
            <section class="row content-home">

              <?php $related = related(get_the_ID());?>
              <?php if( $related->have_posts() ): ?>
                  <?php while ( $related->have_posts() ) : $related->the_post(); ?>

                  <article class="col-xx-1 col-xs-6 col-md-3">
                      <figure>
                        <!-- src="http://placehold.it/257x257" -->
                        <img class="img-responsive" src="<?= wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' )[0]; ?>">
                        <figcaption>
                          <?php the_title(); ?>
                        </figcaption>
                        <a href="<?php the_permalink(); ?>">
                          <div class="excerpt">
                            <h4> <?php the_title(); ?></h4>
                            <p>
                              <?php the_excerpt(); ?>
                            </p>
                            <i class="view-more"></i>
                          </div>
                        </a>
                      </figure>
                  </article>

                  <?php endwhile; ?>
              <?php endif; ?>
              <?php wp_reset_query();?>
            </section>

            <section class="row">
                <h3 class="comments-info">Los comentarios aquí expresados no reflejan la opinión de Volkswagen </h3>
                <div class="comments-all">
                  <!-- Nav tabs -->
                  <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#face" role="tab" data-toggle="tab">Comentar con facebook</a></li>
                    <li><a href="#comment-site" role="tab" data-toggle="tab">Comentar con tu correo</a></li>
                  </ul>

                  <!-- Tab panes -->
                  <div class="tab-content">
                    <div class="tab-pane active" id="face">
                      <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-numposts="2" data-colorscheme="light" width="100%"></div>
                    </div>
                    <div class="tab-pane" id="comment-site">
                      <?php if(is_page() || is_single()) : comments_template( '', true ); endif; ?>
                    </div>
                  </div>
                </div>
            </section>

          </div>