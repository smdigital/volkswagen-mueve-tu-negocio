/* Adobe Tag Container Loader version: 1.1
Copyright 1996-2014 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com */
/*
 * #1: considered multi-suite tagging (comma-sep. list of rs ids)
 * #2: set namespace dynamically dep. on scms_account OR scms.tagContainerNamespace
*/
var ncms=new TagContainerLoader();
ncms.tagContainerDC="d1";
ncms.account=(typeof(scms)!="undefined" && typeof(scms.account)!="undefined")?scms.account:s_account;
ncms.account=(ncms.account.indexOf(",")>-1)?ncms.account.substring(0,ncms.account.indexOf(",")):ncms.account;
ncms.tagContainerNamespace="";
ncms.tagContainerNamespace=(ncms.account.indexOf("vwn")==0)?"vwn":"volkswagenpkw";
if (ncms.tagContainerNamespace=="") {
	if (typeof(scms.tagContainerNamespace)!="undefined") ncms.tagContainerNamespace=scms.tagContainerNamespace;
}
ncms.tagContainerName="NonAdobe_"+ncms.account;
ncms.loadTagContainer();