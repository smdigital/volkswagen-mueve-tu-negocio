jQuery(document).ready(function($) {

//------------------------------------------ Boton folleto ----------------------------------------------

    tinymce.create('tinymce.plugins.wpse72393_plugin', {
        init : function(ed, url) {
                ed.addCommand('wpse72393_insert_shortcode', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();
                    if( selected ){
                        content =  '[folleto imagen="http://localhost/fusion/exportaciones/vw/vw-despliegue-automatico/wp-content/themes/WP-Skeleton-Theme-master/images/preview-pdf.png" pdf="http://www.epa.gov/ttncatc1/dir1/lea-02sp.pdf" ubicacion="left"]'+selected+'[/folleto imagen="http://localhost/fusion/exportaciones/vw/vw-despliegue-automatico/wp-content/themes/WP-Skeleton-Theme-master/images/preview-pdf.png" pdf="http://www.epa.gov/ttncatc1/dir1/lea-02sp.pdf" ubicacion="left"]';
                    }else{
                        content =  '[folleto imagen="http://localhost/fusion/exportaciones/vw/vw-despliegue-automatico/wp-content/themes/WP-Skeleton-Theme-master/images/preview-pdf.png" pdf="http://www.epa.gov/ttncatc1/dir1/lea-02sp.pdf" ubicacion="left"]';
                    }
                    tinymce.execCommand('mceInsertContent', false, content);
                });
            ed.addButton('wpse72393_button', {title : 'Insert folleto pdf', cmd : 'wpse72393_insert_shortcode', image: url + '/image.png' });
        },
    });
    tinymce.PluginManager.add('wpse72393_button', tinymce.plugins.wpse72393_plugin);


//------------------------------------------ Boton imagen ----------------------------------------------

    tinymce.create('tinymce.plugins.wpse72394_plugin', {
        init : function(ed, url) {
                // Register command for when button is clicked
                ed.addCommand('wpse72394_insert_shortcode', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();

                    if( selected ){
                        //If text is selected when button is clicked
                        //Wrap shortcode around it.
                        content =  '[img titulo="Escriba aquí el titulo"  imagen="http://xxx/x.png" pie_de_foto="Aquí el pie de foto" ubicacion="left"]'+selected+'[/img titulo="Escriba aquí el titulo"  imagen="http://xxx/x.png" pie_de_foto="Aquí el pie de foto" ubicacion="left"]';
                    }else{
                        content =  '[img titulo="Escriba aquí el titulo"  imagen="http://xxx/x.png" pie_de_foto="Aquí el pie de foto" ubicacion="left"]';
                    }

                    tinymce.execCommand('mceInsertContent', false, content);
                });

            // Register buttons - trigger above command when clicked
            ed.addButton('wpse72394_button', {title : 'Insert imagen', cmd : 'wpse72394_insert_shortcode', image: url + '/image.png' });
        },
    });

    // Register our TinyMCE plugin
    // first parameter is the button ID1
    // second parameter must match the first parameter of the tinymce.create() function above
    tinymce.PluginManager.add('wpse72394_button', tinymce.plugins.wpse72394_plugin);

//------------------------------------------ Boton cita ----------------------------------------------

    tinymce.create('tinymce.plugins.wpse72395_plugin', {
        init : function(ed, url) {
                ed.addCommand('wpse72395_insert_shortcode', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();

                    if( selected ){
                        content =  '[cita info="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip." ubicacion="right"]'+selected+'[/cita info="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip." ubicacion="right"]';
                    }else{
                        content =  '[cita info="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip." ubicacion="right"]';
                    }

                    tinymce.execCommand('mceInsertContent', false, content);
                });
            ed.addButton('wpse72395_button', {title : 'Insert cita', cmd : 'wpse72395_insert_shortcode', image: url + '/image.png' });
        },
    });
    tinymce.PluginManager.add('wpse72395_button', tinymce.plugins.wpse72395_plugin);


//------------------------------------------ Boton testimonio ----------------------------------------------

    tinymce.create('tinymce.plugins.wpse72398_plugin', {
        init : function(ed, url) {
                ed.addCommand('wpse72398_insert_shortcode', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();

                    if( selected ){
                        content =  '[testimonio imagen="http://xxx/x.png" nombre="Tomás Restrepo" cargo="Vicepresidente Regional Colombia" testimonio="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip."]'+selected+'[/testimonio imagen="http://xxx/x.png" nombre="Tomás Restrepo" cargo="Vicepresidente Regional Colombia" testimonio="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip."]';
                    }else{
                        content =  '[testimonio imagen="http://xxx/x.png" nombre="Tomás Restrepo" cargo="Vicepresidente Regional Colombia" testimonio="Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip."]';
                    }

                    tinymce.execCommand('mceInsertContent', false, content);
                });
            ed.addButton('wpse72398_button', {title : 'Insert testimonio', cmd : 'wpse72398_insert_shortcode', image: url + '/image.png' });
        },
    });
    tinymce.PluginManager.add('wpse72398_button', tinymce.plugins.wpse72398_plugin);



//------------------------------------------ Boton video_youtube ----------------------------------------------

    tinymce.create('tinymce.plugins.wpse72400_plugin', {
        init : function(ed, url) {
                ed.addCommand('wpse72400_insert_shortcode', function() {
                    selected = tinyMCE.activeEditor.selection.getContent();

                    if( selected ){
                        content =  '[video_youtube video="XaNoo-OYbLs" ubicacion="left"]'+selected+'[/video_youtube video="XaNoo-OYbLs" ubicacion="left"]';
                    }else{
                        content =  '[video_youtube video="XaNoo-OYbLs" ubicacion="left"]';
                    }

                    tinymce.execCommand('mceInsertContent', false, content);
                });
            ed.addButton('wpse72400_button', {title : 'Insert video_youtube', cmd : 'wpse72400_insert_shortcode', image: url + '/image.png' });
        },
    });
    tinymce.PluginManager.add('wpse72400_button', tinymce.plugins.wpse72400_plugin);

});