$(document).ready(function(){

	// Menu responsive
	$("#toggle").click(function() {
	  $(this).toggleClass("on");
	  $("#menu").slideToggle();
	});
	// Menu responsive
	// Filtro categorias
	$('#filter').change(function() {
		arrayCategories = $(this).val();
		$("#category_name").val(arrayCategories.toString());
    }).multipleSelect({
    	placeholder: "Todos",
        width: '238px'
    });
    // Filtro categorias
    if($(".internal-post").is('div')) {
	    // Tabs comments
	    $('#myTab a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})
		// Tabs comments

		// Gallery post
		$('.gallery').bxSlider({
			auto: true,
		  	slideWidth: 160,
		  	minSlides: 2,
		  	maxSlides: 4,
		  	slideMargin: 10
		});

		$('.bxbanner').bxSlider({
		  pagerCustom: '#bx-pager'
		});

		$('.lightbox').lightbox();
		// Gallery post
	}

});
