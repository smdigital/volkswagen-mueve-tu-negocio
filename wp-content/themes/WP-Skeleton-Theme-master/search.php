<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */

get_header(); 
get_template_part( 'menu', 'index' ); //the  menu + logo/site title ?>

    <?php if ( have_posts() ) : ?>
            <h2 class="title-general padding-container">Resultados de la búsqueda para: <?= get_search_query() ?></h2>                                    
            <?php get_template_part( 'loop-search', 'search' );  ?>
         
    <?php else : ?>
            <div class="padding-container">
                <h2 class="title-general">No hay resultados de busqueda</h2>
                <p>Lo sentimos, pero nada corresponde a sus criterios de búsqueda. Por favor, inténtelo de nuevo con algunas palabras clave diferentes.</p>
                <form  method="get" id="searchform" action="<?php bloginfo('url'); ?>/" class="search">
                  <input type="text" value="<?php the_search_query(); ?>" name="s" id="s">
                  <input type="submit" value="Buscar">
                </form>
            </div>
    <?php endif; ?>

<?php get_footer(); ?>