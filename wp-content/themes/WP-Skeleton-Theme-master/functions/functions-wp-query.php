<?php
function breadcrumbs_category($cat) {
	$parent = get_category($cat)->category_parent;
	$breadcrumbs = [];
	if (!$parent) {
	  $breadcrumbs['link'] = '<a href="'.get_category_link( $cat ).'">'.get_cat_name($cat).'</a> > Todos';
	  $breadcrumbs['cat'] = get_cat_name($cat);
	}else{
	  $breadcrumbs['link'] = '<a href="'.get_category_link( $parent ).'">'.get_cat_name($parent).'</a> > '.get_cat_name($cat);
	  $breadcrumbs['cat'] = get_cat_name($parent);
	}
	return $breadcrumbs;
}

function categories() {
	$args=array(
	'parent' => 0,
	'orderby' => 'id',
	);
	return get_categories($args);
}

function category_parent($cat) {
	$args=array(
	'parent' => $cat,
	'orderby' => 'name',
	);
	$categories = get_categories($args);
	if (!$categories) {
		$parent = get_category($cat)->category_parent;
		$args=array(
		'parent' => $parent,
		'orderby' => 'name',
		);
		$categories = get_categories($args);
	}
	return $categories;
}

function most_viewed(){
	$day = 30;
	$argsViewed  = array(
		'post_type' => 'post',
		'order' 		=> 'DESC',
		'orderby'   => 'meta_value_num',
		'meta_key'  => '_views',
		'showposts' => 5,
		'date_query' => array(
			array(
				'column' => 'post_date_gmt',
				'after' => $day->days.' day ago',
			)
		),
	);
	return new WP_Query($argsViewed);
}

function post_index() {

	$day = 30;
	$argsDestacado = array(
		'showposts' => 1,
		'post_type' => 'post',
		'meta_key' => 'destacado',
		'meta_value' => 1,
		'date_query' => array(
			array(
				'column' => 'post_date_gmt',
				'after' => $day->days.' day ago',
			)
		),

	);
	$wp_query["highlights"] =  new WP_Query ($argsDestacado);

		// Si no hay destacado en el ultimo mes, trae el mas reciente
	if(!$wp_query["highlights"]->posts[0]->ID){
		$argsDestacado = array(
	   		'showposts' => 1,
		);
		$wp_query["highlights"] =  new WP_Query ($argsDestacado);
	}

	$findDestacado = $wp_query["highlights"]->posts[0]->ID;

	$argsRecent = array(
	    'showposts' => 10,
	);
	$argsRecent['post__not_in'] = array($findDestacado);

	$wp_query["recent"] = new WP_Query ($argsRecent);

	return $wp_query;
}


function related($post){
	$argsRelated = array(
		'showposts' => 4,
		'post__not_in' => array( $post)
	);
	return new WP_Query ($argsRelated);
}