<?php
function longitud_excerpt($length) {
    return 25;
}
add_filter('excerpt_length', 'longitud_excerpt');

function cut_text($texto, $limite=4){
	$arrayTexto = split(' ',$texto);
	$texto = '';
	for ($i=0; $i < $limite; $i++) {
		$texto = $texto.$arrayTexto[$i].' ';
	}
	$texto = $texto.'...';
	return $texto;
}

function views_count(){
	if(is_single()){
		global $post;
		$views = get_post_meta($post->ID, '_views', true);
		if(!$views){
			$views = 1;
		} else {
			$views += 1;
		}
		update_post_meta($post->ID, '_views', $views);
	} // is_single()
}

add_action('wp_head', 'views_count');

// Menus
if ( function_exists( 'register_nav_menus' ) ) {
  	register_nav_menus(
  		array(
  		  'main_menu' => 'Menú principal',
  		)
  	);
}

//Imagen destacada al resumen RSS (Feed)
add_action('rss2_item', function(){
  global $post;

  $output = '';
  $thumbnail_ID = get_post_thumbnail_id( $post->ID );
  $thumbnail = wp_get_attachment_image_src($thumbnail_ID, 'thumbnail');
  $output .= '<post-thumbnail>';
    $output .= '<url>'. $thumbnail[0] .'</url>';
    $output .= '<width>257</width>';
    $output .= '<height>257</height>';
    $output .= '</post-thumbnail>';

  echo $output;
});



			// Tamaño imagenes destacadas
add_image_size('large', 521, 520, true);
add_image_size('medium', 521, 257, true);
add_image_size('thumbnail', 257, 257, true);
add_image_size('internal', 1050, 400, true);