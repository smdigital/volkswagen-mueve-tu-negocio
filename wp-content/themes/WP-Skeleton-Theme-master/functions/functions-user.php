<?php
    //1. Add a new form element...
    add_action( 'register_form', 'myplugin_register_form' );
    function myplugin_register_form() {

        $first_name = ( isset( $_POST['first_name'] ) ) ? trim( $_POST['first_name'] ) : '';
        $country = ( isset( $_POST['country'] ) ) ? trim( $_POST['country'] ) : '';
        
        ?>
        <p>
            <input type="text" name="country" id="country" class="input" value="<?php echo esc_attr( wp_unslash( $country ) ); ?>" placeholder="País" size="25" /></label>
        </p>
        <p>
            <!-- <label for="first_name"><?php //_e( 'First Name', 'mydomain' ) ?><br /> -->
            <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr( wp_unslash( $first_name ) ); ?>" placeholder="Nombre y apellido del usuario" size="25" /></label>
        </p>
        <?php
    }

    //2. Add validation. In this case, we make sure first_name is required.
    add_filter( 'registration_errors', 'myplugin_registration_errors', 10, 3 );
    function myplugin_registration_errors( $errors, $sanitized_user_login, $user_email ) {

        if ( ! isset( $_POST['country'] ) || empty( trim( $_POST['country'] ) ) ) {
            $errors->add( 'country_name_error', __( '<strong>ERROR</strong>: Debe incluir un nombre de país.', 'mydomain' ) );
        }
        if ( ! isset( $_POST['first_name'] ) || empty( trim( $_POST['first_name'] ) ) ) {
            $errors->add( 'first_name_error', __( '<strong>ERROR</strong>: Debes incluir un nombre', 'mydomain' ) );
        }

        return $errors;
    }

    //3. Finally, save our extra registration user meta.
    add_action( 'user_register', 'myplugin_user_register' );
    function myplugin_user_register( $user_id ) {
        if ( isset( $_POST['country'] ) ) {
            update_user_meta( $user_id, 'country', trim( $_POST['country'] ) );
        }
        if ( isset( $_POST['first_name'] ) ) {
            update_user_meta( $user_id, 'first_name', trim( $_POST['first_name'] ) );
        }
    }


// Eliminar la barra de administracion
function my_function_admin_bar($content) { 
return ( current_user_can("administrator") ) ? $content : false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');     