<?php
function mytheme_comment($comment, $args, $depth) {
	// Comentario actual
   $GLOBALS['comment'] = $comment; ?>
   <div <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
   		
	      <div id="comment-<?php comment_ID(); ?>">
                                    <div class="block comments">
                                        <div class="block comment">
                                            <figure class="inline">
                                                <?php echo get_avatar($comment,$size='60'); ?>
                                            </figure>
                                            <article class="inline">
                                                <div class="autor-resp">
                                                	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                                                    <!-- Carlos Muñoz -->
                                                    <span><div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div></span>
                                                </div>
                                                <p><?php comment_text() ?></p>
                                            </article>
                                        </div>
                                        <?php if ($comment->comment_approved == '0') : ?>
								          <em><?php _e('Your comment is awaiting moderation.') ?></em>
								          <br />
								       <?php endif; ?>
                                        <div class="block right comment-resp-button">
                                        	<!-- Responder  -->
                                        	<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                                        	<span>a este comentario</span>
                                        </div>
                                    </div>       
	      </div>    
<?php
}

function alter_comment_form_fields($fields){

$fields['url'] = '';

return $fields;
}
add_filter('comment_form_default_fields','alter_comment_form_fields');
