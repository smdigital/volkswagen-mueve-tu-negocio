<?php

function modificar_main_query($query){
    //Asegurar que es la consulta principal y que es la home
    if( is_home() && $query->is_main_query() ){
        $query->set( 'posts_per_page', '10' );
    }

    if( is_category() && $query->is_main_query() ){
        $query->set( 'posts_per_page', '-1' );
    }
}

add_action('pre_get_posts', 'modificar_main_query');