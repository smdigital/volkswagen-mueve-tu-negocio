<?php
function shortcode_folleto_pdf($atts) {

  extract(shortcode_atts(array(
          'imagen' => '',
          'pdf' => '',
          'ubicacion' => 'left',
 ), $atts));

  $ga = "ga('send', 'event', 'Descargas', 'PDF', '$pdf');";

  $output = '
                   <div class="brochure-pdf block-'.$ubicacion.'" style="float:'.$ubicacion.';">
                    <div class="container-preview">
                      <div class="img-preview" style="background-image: url('.$imagen.')">
                      </div>
                      <a href="'.$pdf.'?lightbox[iframe]=true&lightbox[width]=75p&lightbox[height]=100p" class="lightbox">
                        <div class="excerpt">
                          <i class="view-more"></i>
                        </div>
                      </a>
                    </div>
                    <a href="'.$pdf.'" target="_blank" download="VW Mueve tu negocio" class="button-blue" onClick="'.$ga.'">
                      Descargar
                    </a>
                  </div>';


  return $output;
}
add_shortcode('folleto', 'shortcode_folleto_pdf');


function shortcode_imagen_secilla($atts) {

  extract(shortcode_atts(array(
          'titulo' => '',
          'imagen' => '',
          'pie_de_foto' => '',
          'ubicacion' => 'left',
 ), $atts));

	$output = '
            <div class="col-xs-12 col-sm-6 col-md-6 shortcode-img" style="float:'.$ubicacion.';">
                <section class="block-'.$ubicacion.'">
                    <ul class="post-tags">
                    </ul>
                    <h2>'.$titulo.'</h2>
                    <figure>
                        <img src="'.$imagen.'">
                        <figcaption>'.$pie_de_foto.'</figcaption>
                    </figure>
                </section>
            </div>';

	return $output;
}
add_shortcode('img', 'shortcode_imagen_secilla');


function shortcode_cita($atts) {

  extract(shortcode_atts(array(
          'info' => '',
          'ubicacion' => 'right',
 ), $atts));

  $output = '
            <div class="col-xs-12 col-sm-6 col-md-6" style="float:'.$ubicacion.';">
                <section class="quote border-green block-'.$ubicacion.' margin-top-bottom">
                    <p class="blue-quote" style="color: #7296ac !important;">
                    '.$info.'
                    </p>
                </section>
            </div>';

  return $output;
}
add_shortcode('cita', 'shortcode_cita');


function shortcode_testimonio($atts) {

  extract(shortcode_atts(array(
          'imagen' => '',
          'nombre' => '',
          'cargo' => '',
          'testimonio' => '',
 ), $atts));

  $output = '
            <div class="row">
                <div class="col-xs-12">
                    <article class="testimonio">
                        <h3>TESTIMONIO</h3>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <section class="block-left margin-top">
                        <div class="figure-author">
                            <figure>
                                <img src="'.$imagen.'">
                            </figure>
                            <div class="resalt-post">
                                <p>'.$nombre.'</p>
                                <span>'.$cargo.'</span>
                            </div>
                        </div>
                    </section>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <section class="quote block-right">
                        <p class="normal-quote">
                        '.$testimonio.'
                        </p>
                    </section>
                </div>
            </div>';

  return $output;
}
add_shortcode('testimonio', 'shortcode_testimonio');


function shortcode_video($atts) {

  extract(shortcode_atts(array(
          'video' => 'XaNoo-OYbLs',
          'ubicacion' => 'left',
 ), $atts));

  $output = '
            <div class="col-xs-12 col-sm-6 col-md-6" style="float:'.$ubicacion.';">
                <section class="block-'.$ubicacion.' margin-top">
                    <div class="video-internal">
                        <iframe width="100%" height="200" src="//www.youtube.com/embed/'.$video.'" frameborder="0" allowfullscreen></iframe>
                    </div>
                </section>
            </div>';

  return $output;
}
add_shortcode('video_youtube', 'shortcode_video');



//------------------------------------------ Boton Folleto ----------------------------------------------

 add_action('init', 'wpse72393_shortcode_button_init');
 function wpse72393_shortcode_button_init() {
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;
      add_filter("mce_external_plugins", "wpse72393_register_tinymce_plugin");
      add_filter('mce_buttons', 'wpse72393_add_tinymce_button');
}
function wpse72393_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72393_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}
function wpse72393_add_tinymce_button($buttons) {
            //Add the button ID to the $button array
    $buttons[] = "wpse72393_button";
    return $buttons;
}



//------------------------------------------ Boton imagen ----------------------------------------------
// Shortcodes en el TinyMCE

 // init process for registering our button
 add_action('init', 'wpse72394_shortcode_button_init');
 function wpse72394_shortcode_button_init() {

      //Abort early if the user will never see TinyMCE
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;

      //Add a callback to regiser our tinymce plugin
      add_filter("mce_external_plugins", "wpse72394_register_tinymce_plugin");

      // Add a callback to add our button to the TinyMCE toolbar
      add_filter('mce_buttons', 'wpse72394_add_tinymce_button');
}


//This callback registers our plug-in
function wpse72394_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72394_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}

//This callback adds our button to the toolbar
function wpse72394_add_tinymce_button($buttons) {
            //Add the button ID to the $button array
    $buttons[] = "wpse72394_button";
    return $buttons;
}



//------------------------------------------ Boton cita ----------------------------------------------

add_action('init', 'wpse72395_shortcode_button_init');
 function wpse72395_shortcode_button_init() {
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;
      add_filter("mce_external_plugins", "wpse72395_register_tinymce_plugin");
      add_filter('mce_buttons', 'wpse72395_add_tinymce_button');
}

function wpse72395_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72395_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}

function wpse72395_add_tinymce_button($buttons) {
    $buttons[] = "wpse72395_button";
    return $buttons;
}

//------------------------------------------ Boton cifras ----------------------------------------------

add_action('init', 'wpse72396_shortcode_button_init');
 function wpse72396_shortcode_button_init() {
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;
      add_filter("mce_external_plugins", "wpse72396_register_tinymce_plugin");
      add_filter('mce_buttons', 'wpse72396_add_tinymce_button');
}

function wpse72396_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72396_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}

function wpse72396_add_tinymce_button($buttons) {
    $buttons[] = "wpse72396_button";
    return $buttons;
}

//------------------------------------------ Boton destacado ----------------------------------------------

add_action('init', 'wpse72397_shortcode_button_init');
 function wpse72397_shortcode_button_init() {
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;
      add_filter("mce_external_plugins", "wpse72397_register_tinymce_plugin");
      add_filter('mce_buttons', 'wpse72397_add_tinymce_button');
}

function wpse72397_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72397_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}

function wpse72397_add_tinymce_button($buttons) {
    $buttons[] = "wpse72397_button";
    return $buttons;
}

//------------------------------------------ Boton testimonio ----------------------------------------------

add_action('init', 'wpse72398_shortcode_button_init');
 function wpse72398_shortcode_button_init() {
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;
      add_filter("mce_external_plugins", "wpse72398_register_tinymce_plugin");
      add_filter('mce_buttons', 'wpse72398_add_tinymce_button');
}

function wpse72398_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72398_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}

function wpse72398_add_tinymce_button($buttons) {
    $buttons[] = "wpse72398_button";
    return $buttons;
}


//------------------------------------------ Boton img_texto ----------------------------------------------

add_action('init', 'wpse72399_shortcode_button_init');
 function wpse72399_shortcode_button_init() {
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;
      add_filter("mce_external_plugins", "wpse72399_register_tinymce_plugin");
      add_filter('mce_buttons', 'wpse72399_add_tinymce_button');
}

function wpse72399_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72399_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}

function wpse72399_add_tinymce_button($buttons) {
    $buttons[] = "wpse72399_button";
    return $buttons;
}

//------------------------------------------ Boton video_youtube ----------------------------------------------

add_action('init', 'wpse72400_shortcode_button_init');
 function wpse72400_shortcode_button_init() {
      if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') && get_user_option('rich_editing') == 'true')
           return;
      add_filter("mce_external_plugins", "wpse72400_register_tinymce_plugin");
      add_filter('mce_buttons', 'wpse72400_add_tinymce_button');
}

function wpse72400_register_tinymce_plugin($plugin_array) {
    $plugin_array['wpse72400_button'] =  get_bloginfo('url').'/wp-content/themes/WP-Skeleton-Theme-master/js/shortcode.js';
    return $plugin_array;
}

function wpse72400_add_tinymce_button($buttons) {
    $buttons[] = "wpse72400_button";
    return $buttons;
}