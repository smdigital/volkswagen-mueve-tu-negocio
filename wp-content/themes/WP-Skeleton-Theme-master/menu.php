<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
          <header>
            <figure class="logo">
              <a href="<?= esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/volkswagen-logo.png"></a>
            </figure>


            <div id="toggle">
              <div class="one"></div>
              <div class="two"></div>
              <div class="three"></div>
            </div>



            <div class="button-header-mobile visible-phone">
              <section>
                <div class="social-networks">
                  <a href="https://www.facebook.com/VolkswagenVehiculosComerciales" target="_blank" class="facebook"></a>
                  <a href="https://www.youtube.com/VWComercialesLatam" target="_blank" class="youtube"></a>
                </div>
                <div>
                  <?php if (!is_user_logged_in()): ?>
                    <a href="<?= esc_url( home_url( '/' ) ); ?>/login" class="button-blue">Ingresar</a>
                  <?php else: ?>
                    <a href="<?= esc_url( home_url( '/' ) ); ?>/logout" class="button-blue">Cerrar sesión</a>
                  <?php endif ?>
                </div>
              </section>
            </div>


            <nav id="menu">
              <ul>
                  <li class="visible-phone">
                    <form  method="get" id="searchform" action="<?php bloginfo('url'); ?>/" class="search">
                      <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" required>
                      <input type="submit" value="Buscar">
                    </form>
                  </li>
                  <?php wp_nav_menu(array('menu_id' => 'main_menu', 'container' => '')); ?>
                  <!-- <li><a href="<?= esc_url( home_url( '/' ) ); ?>/">Inicio</a></li> -->
                <?php// foreach (categories() as $category): ?>
                  <!-- <li><a href="<?= esc_url( home_url( '/' ) ); ?>/category/<?= $category->slug ?>"><?= $category->name ?></a></li> -->
                <?php// endforeach ?>
                  <li class="visible-phone"><a href="http://www.volkswagen-comerciales.com/es/Cotizador.html" target="_blank">Cotizar Vehículo</a></li>
              </ul>
            </nav>
            <div class="button-header visible-no-phone">
              <form  method="get" id="searchform" action="<?php bloginfo('url'); ?>/" class="search">
                <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" required>
                <input type="submit" value="Buscar">
              </form>
              <section>
                <div class="social-networks">
                  <a href="https://www.facebook.com/VolkswagenVehiculosComerciales" target="_blank" class="facebook"></a>
                  <a href="https://www.youtube.com/VWComercialesLatam" target="_blank" class="youtube"></a>
                </div>
                <div>
                  <a href="http://www.volkswagen-comerciales.com/es/Cotizador.html" target="_blank" class="button-white">Cotizar Vehículo</a>
                  <?php if (!is_user_logged_in()): ?>
                    <a href="<?= esc_url( home_url( '/' ) ); ?>/login" class="button-blue">Ingresar</a>
                  <?php else: ?>
                    <a href="<?= esc_url( home_url( '/' ) ); ?>/logout" class="button-blue">Cerrar sesión</a>
                  <?php endif ?>
                </div>
              </section>
            </div>
          </header>