<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
          <div class="category padding-container">

            <h3 class="breadcrumbs">
              <?php $breadcrumbs = breadcrumbs_category($cat); ?>
              <?= $breadcrumbs['link']; ?>
            </h3>
            <?php //if ($breadcrumbs['cat'] == "Vehículos"): ?>
              <a href="http://www.volkswagen-comerciales.com/es/models.html" target="_blank" class="button-white right-models">Ver Modelos</a>
            <?php //endif ?>

            <div class="filter">
              <select id="filter" multiple="multiple">
                  <?php foreach (category_parent($cat) as $category): ?>
                    <option value="<?= $category->slug ?>"><?= $category->name ?></option>
                  <?php endforeach ?>
              </select>
              <form  method="get" id="searchCategories" class="searchCategories" action="<?php bloginfo('url'); ?>/">
                <input type="hidden" name="category_name" id="category_name">
                <!-- <input type="submit" value="Buscar"> -->
                <button type="submit" class="button-blue">Filtrar</button>
              </form>
            </div>
            <?php while ( have_posts() ) : the_post(); ?>
              <article class="row post-category">
                  <div class="col-md-4">
                    <?php
                    $catParent = ($getParent = get_category($cat)->parent)?$getParent:$cat;  //Obtener categoria padre,
                    $categoryVW = get_the_category();                                        //Todas las categorias del post
                    foreach ($categoryVW as $value) {
                        if ($value->parent === $catParent) {
                              $nameCategory = $value->name;
                              $slugCategory = $value->slug;
                              break;
                        }
                    }
                    ?>
                    <figure>
                      <?php  $src = get_template_directory_uri().'/includes/scripts/timthumb.php?src='.wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'medium' )[0].'&h=190&w=345'; ?>
                      <a href="<?php the_permalink(); ?><?= (count($categoryVW)>1) ? '?cat='.$slugCategory : '' ?>"><img class="img-responsive" src="<?= $src ?>"></a>
                      <figcaption>
                        <?= $nameCategory ?>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-md-8">
                    <div class="info-post">
                      <h2><a href="<?php the_permalink(); ?><?= (count($categoryVW)>1) ? '?cat='.$slugCategory : '' ?>"><?php the_title(); ?></a></h2>
                      <p><?php the_excerpt(); ?></p>
                      <div class="social-networks">
                        <span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?><?= (count($categoryVW)>1) ? '?cat='.$slugCategory : '' ?>'></span>
                        <span class='st_twitter_large' st_via="" st_msg="<?php the_title(); ?> #MueveTuNegocio" st_title="|" st_url='<?php the_permalink(); ?><?= (count($categoryVW)>1) ? '?cat='.$slugCategory : '' ?>'></span>
                        <span class='st_linkedin_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?><?= (count($categoryVW)>1) ? '?cat='.$slugCategory : '' ?>'></span>
                        <span class='st_email_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?><?= (count($categoryVW)>1) ? '?cat='.$slugCategory : '' ?>'></span>
                      </div>
                    </div>
                  </div>
              </article>
            <?php endwhile; ?>
          </div>