<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
          <footer>
              <div class="more-information">Para mayor información sobre nuestros vehículos visita <a href="//www.volkswagen-comerciales.com/es.html" target="_blank">www.volkswagen-comerciales.com</a></div>
              <figure class="logo-footer">
                <img src="<?php echo get_template_directory_uri(); ?>/img/icon/volkswagen-footer.png">
                <a href="<?= esc_url( home_url( '/' ) ); ?>/terminos-y-condiciones">Términos y condiciones</a>
              </figure>
          </footer>
    </div><!-- /.container -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery-migrate-1.2.1.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/widgets/bootstrap-3/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/widgets/bxslider/jquery.bxslider.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/widgets/ligthbox-evolution/jquery.lightbox.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/includes/widgets/multiple-select-master/jquery.multiple.select.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

	<?php wp_footer(); ?>
  <?php get_template_part( 'omniture', 'footer' ); ?>
	</body>
</html>