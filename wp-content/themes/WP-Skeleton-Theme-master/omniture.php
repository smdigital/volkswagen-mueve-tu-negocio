<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>

<script>
	$(".button-header .social-networks a").on("click", function(event){
		scms.linkName = "Mueve tu Negocio : <Section> : Social media : <network>";
		scms.eVar8="carlineGroupName";
		scms.eVar9="carlineName";
		scms.eVar10="equipmentlineName";
		scms.eVar13="modelName";
		scms.prop41="CarlineGroupName";
	});

	$(".button-header .button-white").on("click", function(event){
		scms.linkName = "Mueve tu Negocio : <Section> : Request test drive";
		scms.eVar8="carlineGroupName";
		scms.eVar9="carlineName";
		scms.eVar10="equipmentlineName";
		scms.eVar11="FunctionName";
		scms.eVar13="modelName";
		scms.prop33="ApplicationAndAction";
		scms.prop34="ApplicationAndActionAndCarlineGroupName";
		scms.prop41="CarlineGroupName";
	});	

	$(".button-header .button-blue").on("click", function(event){
		scms.linkName = "Mueve tu Negocio : <Section> : Login";
		scms.eVar8="carlineGroupName";
		scms.eVar9="carlineName";
		scms.eVar10="equipmentlineName";
		scms.eVar11="FunctionName";
		scms.eVar13="modelName";
		scms.prop33="ApplicationAndAction";
		scms.prop34="ApplicationAndActionAndCarlineGroupName";
		scms.prop41="CarlineGroupName";
	});	

	$("#loginform #wp-submit").on("click", function(event){
		scms.linkName = "Mueve tu Negocio : <Section> : <sub-section> : Login";
		scms.eVar8="carlineGroupName";
		scms.eVar9="carlineName";
		scms.eVar10="equipmentlineName";
		scms.eVar11="FunctionName";
		scms.eVar13="modelName";		
		scms.prop41="CarlineGroupName";
	});		

</script>

<?php if (is_page()): ?>
	
	<?php $idPage = get_the_id(); ?>

<!-- Inicio -->
	<?php if ($idPage == 2): ?>
	    <script type="text/javascript">
			//<![CDATA[
			var scms=s_gi("vwnlam");
			scms.trackingServer="metric.volkswagen-nutzfahrzeuge.de";
			scms.trackingServerSecure="smetric.volkswagen-nutzfahrzeuge.de";
			scms.visitorNamespace="vwn";
			scms.dc=122;
			scms.account="vwnlam";
			scms.currencyCode="EUR";
			scms.charSet="UTF-8";
			scms.trackExternalLinks=true;
			scms.linkInternalFilters="javascript:,lh,localhost,origin.vwn-lam.d4.volkswagen-nutzfahrzeuge.com,origin.author.cms.volkswagen.de,volkswagen-comerciales.com,volkswagen-cv-la.com,volkswagen-commercial.com";
			scms.linkExternalFilters="";
			scms.linkLeaveQueryString=false;
			scms.trackDownloadLinks=true;
			scms.linkDownloadFileTypes="exe,zip,wav,mp3,mp4,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
			scms.linkTrackVars="None";
			scms.linkTrackEvents="None";
			scms.trackInlineStats=true;
			scms.channel="CMS";
			scms.eVar1="CMS-ES-LAM";
			scms.prop8="CMS-ES-LAM";
			scms.prop32="CMS";
			scms.pageName="CMS : Mueve tu Negocio : home";
			scms.eVar8="carlineGroupName";
			scms.eVar9="carlineName";
			scms.eVar10="equipmentlineName";
			scms.eVar13="modelName";
			scms.prop34="ApplicationAndActionAndCarlineGroupName";
			scms.prop41="CarlineGroupName";
			scms.prop42="CarlineGroupNameAndPagename";		
	    //]]></script>
	<?php endif ?>

<!-- Términos y condiciones -->
	<?php if ($idPage == 186): ?>
	    <script type="text/javascript">
			//<![CDATA[
			var scms=s_gi("vwnlam");
			scms.trackingServer="metric.volkswagen-nutzfahrzeuge.de";
			scms.trackingServerSecure="smetric.volkswagen-nutzfahrzeuge.de";
			scms.visitorNamespace="vwn";
			scms.dc=122;
			scms.account="vwnlam";
			scms.currencyCode="EUR";
			scms.charSet="UTF-8";
			scms.trackExternalLinks=true;
			scms.linkInternalFilters="javascript:,lh,localhost,origin.vwn-lam.d4.volkswagen-nutzfahrzeuge.com,origin.author.cms.volkswagen.de,volkswagen-comerciales.com,volkswagen-cv-la.com,volkswagen-commercial.com";
			scms.linkExternalFilters="";
			scms.linkLeaveQueryString=false;
			scms.trackDownloadLinks=true;
			scms.linkDownloadFileTypes="exe,zip,wav,mp3,mp4,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
			scms.linkTrackVars="None";
			scms.linkTrackEvents="None";
			scms.trackInlineStats=true;
			scms.channel="CMS";
			scms.eVar1="CMS-ES-LAM";
			scms.prop8="CMS-ES-LAM";
			scms.prop32="CMS";
			scms.pageName="CMS : Mueve tu Negocio : Terms and conditions";
			scms.eVar8="carlineGroupName";
			scms.eVar9="carlineName";
			scms.eVar10="equipmentlineName";
			scms.eVar13="modelName";
			scms.prop34="ApplicationAndActionAndCarlineGroupName";
			scms.prop41="CarlineGroupName";
			scms.prop42="CarlineGroupNameAndPagename";		
	    //]]></script>
	<?php endif ?>

<?php endif ?>

<?php if (is_search()): ?>
<!-- Búsqueda -->
	    <script type="text/javascript">
			//<![CDATA[
			var scms=s_gi("vwnlam");
			scms.trackingServer="metric.volkswagen-nutzfahrzeuge.de";
			scms.trackingServerSecure="smetric.volkswagen-nutzfahrzeuge.de";
			scms.visitorNamespace="vwn";
			scms.dc=122;
			scms.account="vwnlam";
			scms.currencyCode="EUR";
			scms.charSet="UTF-8";
			scms.trackExternalLinks=true;
			scms.linkInternalFilters="javascript:,lh,localhost,origin.vwn-lam.d4.volkswagen-nutzfahrzeuge.com,origin.author.cms.volkswagen.de,volkswagen-comerciales.com,volkswagen-cv-la.com,volkswagen-commercial.com";
			scms.linkExternalFilters="";
			scms.linkLeaveQueryString=false;
			scms.trackDownloadLinks=true;
			scms.linkDownloadFileTypes="exe,zip,wav,mp3,mp4,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
			scms.linkTrackVars="None";
			scms.linkTrackEvents="None";
			scms.trackInlineStats=true;
			scms.channel="CMS";
			scms.eVar1="CMS-ES-LAM";
			scms.prop8="CMS-ES-LAM";
			scms.prop32="CMS";
			scms.pageName="CMS : Mueve tu Negocio : Search";
			scms.eVar8="carlineGroupName";
			scms.eVar9="carlineName";
			scms.eVar10="equipmentlineName";
			scms.eVar13="modelName";
			scms.prop15="SearchCriteria";
			scms.prop34="ApplicationAndActionAndCarlineGroupName";
			scms.prop41="CarlineGroupName";
			scms.prop42="CarlineGroupNameAndPagename";	
	    //]]></script>
<?php endif ?>

<?php if (is_category()): ?>

	<?php if ($cat <= 5): ?>	
<!-- Categorias -->
	    <script type="text/javascript">
			//<![CDATA[
			var scms=s_gi("vwnlam");
			scms.trackingServer="metric.volkswagen-nutzfahrzeuge.de";
			scms.trackingServerSecure="smetric.volkswagen-nutzfahrzeuge.de";
			scms.visitorNamespace="vwn";
			scms.dc=122;
			scms.account="vwnlam";
			scms.currencyCode="EUR";
			scms.charSet="UTF-8";
			scms.trackExternalLinks=true;
			scms.linkInternalFilters="javascript:,lh,localhost,origin.vwn-lam.d4.volkswagen-nutzfahrzeuge.com,origin.author.cms.volkswagen.de,volkswagen-comerciales.com,volkswagen-cv-la.com,volkswagen-commercial.com";
			scms.linkExternalFilters="";
			scms.linkLeaveQueryString=false;
			scms.trackDownloadLinks=true;
			scms.linkDownloadFileTypes="exe,zip,wav,mp3,mp4,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
			scms.linkTrackVars="None";
			scms.linkTrackEvents="None";
			scms.trackInlineStats=true;
			scms.channel="CMS";
			scms.eVar1="CMS-ES-LAM";
			scms.prop8="CMS-ES-LAM";
			scms.prop32="CMS";
			scms.pageName="CMS : Mueve tu Negocio : <Section>";
			scms.eVar8="carlineGroupName";
			scms.eVar9="carlineName";
			scms.eVar10="equipmentlineName";
			scms.eVar13="modelName";			
			scms.prop41="CarlineGroupName";
			scms.prop42="CarlineGroupNameAndPagename";
	    //]]></script>
	<?php else: ?>
<!-- Subcategorias -->
	    <script type="text/javascript">
			//<![CDATA[
			var scms=s_gi("vwnlam");
			scms.trackingServer="metric.volkswagen-nutzfahrzeuge.de";
			scms.trackingServerSecure="smetric.volkswagen-nutzfahrzeuge.de";
			scms.visitorNamespace="vwn";
			scms.dc=122;
			scms.account="vwnlam";
			scms.currencyCode="EUR";
			scms.charSet="UTF-8";
			scms.trackExternalLinks=true;
			scms.linkInternalFilters="javascript:,lh,localhost,origin.vwn-lam.d4.volkswagen-nutzfahrzeuge.com,origin.author.cms.volkswagen.de,volkswagen-comerciales.com,volkswagen-cv-la.com,volkswagen-commercial.com";
			scms.linkExternalFilters="";
			scms.linkLeaveQueryString=false;
			scms.trackDownloadLinks=true;
			scms.linkDownloadFileTypes="exe,zip,wav,mp3,mp4,mov,mpg,avi,wmv,pdf,doc,docx,xls,xlsx,ppt,pptx";
			scms.linkTrackVars="None";
			scms.linkTrackEvents="None";
			scms.trackInlineStats=true;
			scms.channel="CMS";
			scms.eVar1="CMS-ES-LAM";
			scms.prop8="CMS-ES-LAM";
			scms.prop32="CMS";
			scms.pageName="CMS : Mueve tu Negocio : <Section> : <sub-section>";
			scms.eVar8="carlineGroupName";
			scms.eVar9="carlineName";
			scms.eVar10="equipmentlineName";
			scms.eVar13="modelName";	
			scms.prop34="ApplicationAndActionAndCarlineGroupName";		
			scms.prop41="CarlineGroupName";
			scms.prop42="CarlineGroupNameAndPagename";
	    //]]></script>

	<?php endif ?>

<?php endif ?>


<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/omniture/market.js"></script><script type="text/javascript">
//<![CDATA[
jQuery(document).ready(function () {
vwd4.config.onLoadTrack({"onLoadTrackingDisabled":false,"onLoadTrackingFallbackOnly":false});
});
//]]></script><noscript><img src="//metric.volkswagen-nutzfahrzeuge.de/b/ss/vwnlam/5/0?ch=CMS&amp;v1=CMS-ES-LAM&amp;c8=CMS-ES-LAM&amp;c32=CMS&amp;pageName=CMS+%3A+Prueba+de+Manejo&amp;hier1=CMS%3APrueba+de+Manejo&amp;v11=None&amp;v50=fallback-noscript&amp;events=None&amp;v9=None&amp;v10=None&amp;v13=None&amp;v14=None&amp;v41=None&amp;v42=None&amp;v8=None&amp;c42=CMS+%3A+None+%3A+Mueve+tu+Negocio"/></noscript>
