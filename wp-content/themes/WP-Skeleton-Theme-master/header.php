<?php
/**
 * @package WordPress
 * @subpackage WP-Skeleton
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if gte IE 9 ]><html class="no-js ie9" lang="en"> <![endif]-->

    <title><?php wp_title('|',true,'right'); ?></title>

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Mobile Specific Metas
  	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="fb:admins" content="1292066195"/>
	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
	<!-- Stylesheets
	================================================== -->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/fonts.css">

    <link href="<?php echo get_template_directory_uri(); ?>/includes/widgets/bootstrap-3/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/includes/widgets/multiple-select-master/multiple-select.css" />

    <link href="<?php echo get_template_directory_uri(); ?>/includes/widgets/bxslider/jquery.bxslider.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/includes/widgets/ligthbox-evolution/jquery.lightbox.css">

    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
    <!-- <link rel="stylesheet/less" href="<?php echo get_template_directory_uri(); ?>/css/main.less">
    <link rel="stylesheet/less" href="<?php echo get_template_directory_uri(); ?>/css/main-responsive.less"> -->

    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/prefixfree.min.js"></script>
    <!--<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/less.js"></script>-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>><!-- the Body  -->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/omniture/s_code_HQ.js.js"></script>
<div class="container">
