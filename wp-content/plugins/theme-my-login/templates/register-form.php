<?php
/*
If you would like to edit this file, copy it to your current theme's directory and edit it there.
Theme My Login will always look in your theme's directory first, before using this default template.
*/
?>
<div class="login" id="theme-my-login<?php $template->the_instance(); ?>">
	<?php //$template->the_action_template_message( 'register' ); ?>

	<section class="row register">            
        <div class="col-md-7">
			<h4>Crea tu cuenta en el blog de Volkswagen Vehículos Comerciales</h4>

			<section class="row benefits">            
		        <div class="col-xx-1 col-xs-4">
		        	<figure class="text-center">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/icon/opina.png">
		        	</figure>
		        	<p><strong>Opina </strong> sobre los temas que te interesan.</p>
		        </div>
		        <div class="col-xx-1 col-xs-4">
		        	<figure class="text-center">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/icon/download.png">
		        	</figure>
		        	<p><strong>Descarga </strong>tu contenido favorito.</p>		        	
		        </div>
		        <div class="col-xx-1 col-xs-4">
		        	<figure class="text-center">
		        		<img src="<?php echo get_template_directory_uri(); ?>/img/icon/updates.png">
		        	</figure>	
		        	<p><strong>Recibe </strong>actualizaciones de tus temas de interés.</p>	        	
		        </div>
		    </section>

		    <p class="register-facebook">Si ya tienes cuenta <a href="<?= esc_url( home_url( '/' ) ); ?>/login">ingresa aquí </a>o regístrate diligenciando este formulario o con tu perfil de Facebook.</p>
		    <?php do_action('oa_social_login'); ?>

			<?php $template->the_errors(); ?>
			<form name="registerform" id="registerform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'register' ); ?>" method="post">
				<p>
					<!-- <label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Username' ); ?></label> -->
					<input type="text" name="user_login" id="user_login<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_login' ); ?>" placeholder="Nombre de usuario" size="20" />
				</p>
				<p>
					<!-- <label for="user_email<?php $template->the_instance(); ?>"><?php _e( 'E-mail' ); ?></label> -->
					<input type="text" name="user_email" id="user_email<?php $template->the_instance(); ?>" class="input" value="<?php $template->the_posted_value( 'user_email' ); ?>" placeholder="e-mail" size="20" />
				</p>

				<?php do_action( 'register_form' ); ?>

				<!-- <p id="reg_passmail<?php $template->the_instance(); ?>"><?php //echo apply_filters( 'tml_register_passmail_template_message', __( 'A password will be e-mailed to you.' ) ); ?></p> -->

				<div class="checkbox">
				   <label>
				     <input type="checkbox" required> Acepto voluntariamente los <a href="<?= esc_url( home_url( '/' ) ); ?>/terminos-y-condiciones">términos y condiciones</a> de Volkswagen Vehículos Comerciales.
				   </label>
				</div>

				<p class="submit">
					<input type="submit" name="wp-submit" id="wp-submit<?php $template->the_instance(); ?>" value="Crear tu cuenta" />
					<input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'register' ); ?>" />
					<input type="hidden" name="instance" value="<?php $template->the_instance(); ?>" />
					<input type="hidden" name="action" value="register" />
				</p>
			</form>
			<?php $template->the_action_links( array( 'register' => false ) ); ?>
        </div>
        <div class="col-md-5">
        </div>
    </section>
</div>
